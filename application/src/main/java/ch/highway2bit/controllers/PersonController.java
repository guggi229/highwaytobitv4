package ch.highway2bit.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import ch.highway2bit.models.Person;

@Controller
public class PersonController {
 
	@GetMapping("/person")
    public ModelAndView showForm(Model model) {
        return new ModelAndView("person", "person", new Person());
    }
 
    @PostMapping(value = "/person")
    public String submit(@ModelAttribute Person person) {
        
        return "personView";
    }
}