package ch.highway2bit.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller

public class HelloWorldController {

	@RequestMapping("/showForm")
	public String showForm() {
			return "form";
	}

	@RequestMapping("/processForm")
	public String processForm() {
		
		
		
		return "hello";
	}

}