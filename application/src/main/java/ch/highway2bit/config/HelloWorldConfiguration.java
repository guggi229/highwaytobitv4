package ch.highway2bit.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "ch.highway2bit")
public class HelloWorldConfiguration {

	@Bean(name="HelloWorld")
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");

		System.out.println("***********");

		System.out.println("****views******");

		System.out.println("***********");

		System.out.println("***********");

		System.out.println("***********");

		System.out.println("***********");

		System.out.println("***********");
		return viewResolver;
	}

	  public void addResourceHandlers(ResourceHandlerRegistry registry) {

			System.out.println("***********");

			System.out.println("****Resource******");

			System.out.println("***********");

			System.out.println("***********");

			System.out.println("***********");

			System.out.println("***********");

			System.out.println("***********");

			registry.addResourceHandler("/assets/**").addResourceLocations("/assets/");
	    }

}